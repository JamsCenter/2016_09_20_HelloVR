﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class LookAtInEditor : MonoBehaviour {

    public Transform _targetToLookAt;
	// Use this for initialization
	void Start ()
    {
        LookAtTarget();

    }

#if UNITY_EDITOR
    void Update () {

        LookAtTarget();
    }
    void OnValidate() {
        LookAtTarget();
    }
#endif
    void LookAtTarget() {
        if(_targetToLookAt!=null)
        this.transform.LookAt(_targetToLookAt);
    }
}
